##This space is to document anything within the assessment that does not fit into any of the other three repositories.


### Setting up timed instances 

As a team we decided that the instances should only be up and running between certain times (08:30-17:00). Running instances during these working hours can reduce costs by up to 70% compared with running them 24 hours. We thought that this would be beneficial to the client and so decided to use this tool.

If these hours need to be changed:

- Log into AWS and navigate to DynamoDB
- Ensure that your regiion (in the top right) is set to N.Virginia
- To the left is a list of various items, select 'Tables'
- Click on the blue part of the table named 'dynamic-test-stack-ConfigTable-VUAIA2NNP0OI'
- Click 'Explore table items'
- Here you will see the various item i.e. the settings for the schedule
- Click on the one named 'office-hours', here you can change the times that the instances will be running
- Save these changes and you're good to go! (Check instances to ensure that the changes have taken effect)

If you would like to add an instance to this schedule:

- Select the instance you would like to add
- Click, 'Tags', 'Manage tags' and then 'Add tag'
- In the Key section type 'Schedule' and in the Value section type 'office-hours'
- This instance will now run on office hours :)



### Backing up instances 

Since a lot of data is being stored on the PetClinic server, we thought it would be a good idea to have this server backed up. This was done via tha RDS tool in AWS and is done automatically. A snapshot of the instance can be taken to use but is not imperative. 



### Wordpress

We knew that Feranando loved powerwalking and so we created a Wordpress blog for him to share his passion online! It is installed on an instance called amalia-wp and uses Route 53 so the blog can be found at:
 http://fernandosblog.academy.labs.automationlogic.com/

 or by starting the instance called and using the instance's public IP in a browser tab.
 
Log in details:
Wordpress: user = fernando, password = powerWalK123!

(I have to admit that I made a Wordpress account with my (Amalia) work email address and it created an account for me. There was somewhere to log in using the above details but I can't find the page that asked for them last time.)


### Jenkins

In a new browser tab, paste http://52.31.250.147:8080/
The username is: dynamicdevops
The password is: cohort9



### Presentation

For some reason I couldn't add the presentation as a link on the submission area of the assessment but this is the link to the slideshow that we used to present our work:

https://docs.google.com/presentation/d/1mW8Hb5r0CKwCfjgksYbFH5_rEflE1b_b_yw3O4161Dw/edit#slide=id.g1066cfb5581_0_227