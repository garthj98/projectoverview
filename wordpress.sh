# This file contains the commands used to set up a wordpress account. Since it was last minute, it cannot be used as a script to be run but the commands can be used inidivdually.

# Prerequisites:
# - An Amazon Linux machine, I used Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type and gave it the Cohort9-homeips security group.


# Next you need to ssh into your machine and run the folloiwing commands:

sudo su
# Provides root access

# The following commands install the Apache Web Server to run PHP

sudo yum -y install python-simplejson
# Install PHP latest version

sudo yum update
# System wide upgrade

sudo yum install -y default-jre
# Install Java (just to be safe)

sudo yum install httpd
# Install HTTPD server

service httpd start
# Start the Apache web server



# Install PHP to run WordPress:


yum install php php-mysql
# Installs PHP

service httpd restart
# Restart the Apache web server

cd /var/www/html
# Change directory

vi test.php
# This is to test the installation of PHP
# Here you need to press 'i' to enter insert mode and paste: <?php phpinfo() ?>
# You then press esc then :wq to exit vim

# Install mariadb:
sudo yum install mariadb-server -y
sudo systemctl enable mariadb
sudo systemctl start mariadb

wget https://wordpress.org/latest.tar.gz
# Download the latest WordPress installation package

tar -xzf latest.tar.gz
# Unzip and unarchive the installation package to a folder called wordpress




# Now to create a database for Wordpress

mysql -u root -p
# Log into the database as a root user. Press enter once when prompted for a password (this means no password has been set)

CREATE USER 'fernando'@'localhost' IDENTIFIED BY 'powerwalk';
# Creates a user and password for the MySQL database

CREATE DATABASE `wordpress-db`;
# Creates a database called wordpress-db

GRANT ALL PRIVILEGES ON `wordpress-db`.* TO "fernando"@"localhost";
# Grant full privileges for your database to the WordPress user

FLUSH PRIVILEGES;
# Flush the database privileges to pick up all changes

exit
# Exit MySQL



cp wordpress/wp-config-sample.php wordpress/wp-config.php
# This makes a copy of one file, we're going to edit the copy and keep the original as a back-up

nano wordpress/wp-config.php
# Vim may also be used here but I prefer nano

# The next part take a bit of searching. Find where the database is defined, it should look like this:
# define( 'DB_NAME', 'database_name_here' );
# You want to change the second part to the name of the database that has been created; in this case it's wordpress-db
# The next two lines also need to be altered so that the created user and password are in the file.

# Next find the section called Authentication Unique Keys and Salts. Keep going until you get to the line:
# define( 'AUTH_KEY',         'put your unique phrase here' );
# I used the following link which provided me with a list of randomly generated strings that I can use as my keys. I pasted each of them into the spaces provided; keeping the single quotes.
# Save the file and exit the text editor.

cp -r wordpress/* /var/www/html/
# Makes a copy of the contents of the above directory. This means that Wordpress will run at the root

sudo nano /etc/httpd/conf/httpd.conf
# Opens the text editor ready for some changes
# Find the section that starts with <Directory "/var/www/html"> (quite a way).
# Change the AllowOverride None line to read AllowOverride All.
# Be sure that you change the right part, the lines directly above it read:
 #   Options FileInfo AuthConfig Limit
    #
# Save and exit



sudo chown -R apache /var/www
# Grant file ownership of /var/www and its contents to the apache user

sudo chgrp -R apache /var/www
# Grant group ownership of /var/www and its contents to the apache group

sudo chmod 2775 /var/www
find /var/www -type d -exec sudo chmod 2775 {} \;
# Change the directory permissions of /var/www and its subdirectories to add group write permissions and to set the group ID on future subdirectories

find /var/www -type f -exec sudo chmod 0664 {} \;
# Recursively change the file permissions of /var/www and its subdirectories to add group write permissions

sudo systemctl restart httpd
# Restart the Apache web server to pick up the new group and permissions

sudo systemctl enable httpd && sudo systemctl enable mariadb
# Use the systemctl command to ensure that the httpd and database services start at every system boot

sudo systemctl status mariadb
# Verify that the database server is running

sudo systemctl start mariadb
# Ensure that the database service is running

sudo systemctl status httpd
# Verify that your Apache web server (httpd) is running

sudo systemctl start httpd
# Ensure that the https service is running.

# Now when you paste the public IP of your instance, the Wordpress installation page should come up and you can follow the steps on there to get all set up